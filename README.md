# webserv

# About

School 42 team project meant to reproduce a web server with the HTTP protocol. \
It is built in C++, and uses C function for sockets handling. \
It's working on UNIX system.

## How to run it

```
git clone https://gitlab.com/Ralphiki/webserv.git
cd webserv
make
./webserv conf/default.conf
```

Config file validation is based on NGINX's one \
To create a valid server, you'll need at least to provide it :

```
server {
    listen          8080;
    server_name     example.com;
    root            ./www/website;
    index           index.html;

    location / {
        allowed_methods     GET POST;
    }
}
```

You can create as many servers as you wish as long as you choose a different port number for each one

## Ressources

```
# config file will be based on nginx default config
https://www.nginx.com/resources/wiki/start/topics/examples/full/
# socket programming in C
https://aticleworld.com/socket-programming-in-c-using-tcpip/
# How to Load a Webpage in C
https://www.youtube.com/watch?v=CymFHNqC7n4
# Web programming (CGI) in C ( from 8:40 )
https://www.youtube.com/watch?v=rFaRFCyewpA
# CGI 101
http://www.cgi101.com/book/ch3/text.html
```

original repository : https://github.com/m3zh/webserv
